# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-jakobmattsson--jquery-elastic/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-jakobmattsson--jquery-elastic"
  spec.version       = RailsAssetsJakobmattssonJqueryElastic::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "Jan Jarfalk's excellent jquery-elastic plugin"
  spec.summary       = "Jan Jarfalk's excellent jquery-elastic plugin"
  spec.homepage      = "http://www.unwrongest.com/projects/elastic"
  spec.license       = "http://www.opensource.org/licenses/mit-license.php"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_dependency "rails-assets-jquery", ">= 1.2.6"
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
